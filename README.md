**All issues and contributions should be done on
[Gitlab](https://gitlab.com/Dom31/python-project-template). Github
is used only as a mirror for visibility**

# Python Package Template

[![pipeline status](https://gitlab.com/Dom31/python-project-template/badges/master/pipeline.svg)](https://gitlab.com/Dom31/python-project-template/commits/master)
[![coverage report](https://gitlab.com/Dom31/python-project-template/badges/master/coverage.svg)](https://gitlab.com/Dom31/python-project-template/commits/master)

This project is an example of implementation of:

 - documentation ([sphinx](http://www.sphinx-doc.org/en/stable/), selfhosted + [readthedocs](https://readthedocs.org/))
 - testing ([pytest](https://docs.pytest.org/en/latest/)) and coverage ([pytest-cov](https://github.com/pytest-dev/pytest-cov))
 - building a package (`setup.py`, `README.md`, `CHANGELOG.md`, `LICENSE.md`)
 - command line interface with argparse
 - badges for testing, packages, and documentation

Thank's to these site authors:
- https://gitlab.com/costrouc/python-package-template
- https://pypi.org/project/python_boilerplate_template/

__Examples and comments:__
- https://docs.astropy.org/en/stable/index.html
- https://stackoverflow.com/questions/193161/what-is-the-best-project-structure-for-a-python-application


## Assumptions:

Gitlab will be used for the continuous deployment. See these features:

 - [pages](https://docs.gitlab.com/ee/user/project/pages/index.html)
 - [CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/)

## Contributing

All contributions, bug reports, bug fixes, documentation improvements,
enhancements and ideas are welcome. These should be submitted at the
[Gitlab repository](https://gitlab.com/Dom31/python-package-template). Github is only used for visibility.

The goal of this project is to in an opinionated way guide modern
python packaging development for myself.

## License

No licence (but why not : BSD, MIT, GPL v3,...)


# Installation and usage


## Requirements

[requirements.txt](/requirements.txt)

**Note**: others external python modules may be mentioned in [setup.py](/setup.py) and [docs/conf.py](/docs/conf.py) 

## Install with git

```git
git clone https://gitlab.com/Dom31/python-package-template/python-package-template.git
```

## Build

For a deployment on PCs under windows:
```
$ python setup.py install --prefix=C:\Users\pc\deploy  # all the windows binaries under \Users\pc\deploy
$ python setup.py bdist_msi  # people_gui-0.1-amd64 under ./dist
```

## Tests

```
pytest --cov=people tests/
```

See also the section on tests in [.gitlab-ci.yml](/.gitlab-ci.yml)
**Note:** Batch file for windows (coverage output in HTML) : [tests/people-cli/run_coverage_windows.bat](/tests/people-cli/run_coverage_windows.bat)

## logbook messages

Logging is done for the following purposes:
 
    Information gathering
    Troubleshooting
    Generating statistics
    Auditing
    Profiling
    
A [logging.yaml](/people/logging.yaml)  has been implemented as recommended in http://zetcode.com/python/logging/

Thank's to this file, it is possible to choose the logbook type:
```
loggers:
  log_in_console:
    handlers: [console]
  log_in_console_and_file:
    handlers: [console_debug, file_handler]
  log_in_file:
    handlers: [file_handler]
root:
  handlers: [console_debug, file_handler]
  level: DEBUG
```

Python code to read local data file (and compliant with cx_freeze for Windows deployment)
```python

def find_data_file(filename):
    if getattr(sys, 'frozen', False):
        # The application is frozen
        datadir = os.path.dirname(sys.executable)
    else:
        # The application is not frozen
        # Change this bit to match where you store your data files:
        datadir = os.path.dirname(__file__)

    return os.path.join(datadir, filename)
```

Usage:
```python
        menu_file = find_data_file("menu.ui")
        builder.add_from_file(menu_file)
```

More in 
* https://gist.github.com/kingspp/9451566a5555fb022215ca2b7b802f19
* https://stackoverflow.com/questions/38323810/does-pythons-logging-config-dictconfig-apply-the-loggers-configuration-setti

## Package data and meta data

If you are going to publish your package, then you probably want to give your potential users some more information about your package, including a description,
the name of the author or maintainer, and the url to the package's home page. You can find a complete list of all allowed metadata in the setuptools docs.

The [setup.py](/setup.py)  file has been filled as recommended in https://blog.godatadriven.com/setup-py.    

The data file [people.json](/peopleGUI/people.json) is read thank's to the package 

```python
from pkg_resources import resource_stream
```

More in https://stackoverflow.com/questions/6028000/how-to-read-a-static-file-from-inside-a-python-package

## GUI

**Note on GUI**: pygubu is used for its praticity: https://github.com/alejandroautalan/pygubu

Its designer is very simple to use:
```
pygubu-designer
```
Using code from https://github.com/beenje/tkinter-logging-text-widget it is possible to print log messages in a *TK widget console*

```python
    logger.debug('debug message')
    logger.info('info message')
    logger.warning('warn message')
    logger.error('error message')
    logger.critical('critical message')
```


## Usages

```
python people/people_cli.py 
python people/people_cli.py init
python people/people_cli.py file data/people.json
python peopleGUI/people_gui.py
```


## Bug reports


Please report bugs and feature requests at https://gitlab.com/Dom31/python-project-template/issues


Documentation
=============

See the command lines in the section on documentation in [.gitlab-ci.yml](/.gitlab-ci.yml)

The full documentation for CLI and API is available on [Read-the-Docs](https://a-python-project-template.readthedocs.io/en/latest/index.html)

The configuration is in [.readthedocs.yml](/.readthedocs.yml) and [docs/conf.py](/docs/conf.py) 

**Note**: Documentation is generated with the [docs/source/Makefile](/docs/source/Makefile) (Linux)  and [docs/source/Make.bat](/docs/source/Make.bat) (Windows).

TODO: use [sphinx-automodapi](https://sphinx-automodapi.readthedocs.io/en/latest/index.html) in order to get a PDF Design Document with API classes

## Code-Style

We use black as code formatter, so you'll need to format your changes using the
[black code formatter](https://github.com/python/black)

Just run

```
    cd python-gitlab/
    pipinstall --user black
    black .
```
  
to format your code according to our guidelines.
