# encoding: utf8
# peopleGUI.py
import json
import os
import sys
import logging
import logging.config
import tkinter as tk
from tkinter import messagebox, filedialog
from yaml import safe_load
from pkg_resources import resource_stream

import pygubu
from pkg_resources import resource_filename

import peopleGUI.console_gui
#from peopleGUI.text_handler import TextHandler
from people.Human import Human
from people.ListOfHumans import ListOfHumans

# logger = logging.getLogger(__name__)

"""

GUI app for people...

TODO: add docstrings header
"""


class MyApplication(pygubu.TkApplication):
    def _create_ui(self):
        # 1: Create a builder
        self.builder = builder = pygubu.Builder()

        # 2: Load an ui file
        menu_file = find_data_file("menu.ui")
        builder.add_from_file(menu_file)

        # 3: Create the widget using self.master as parent
        self.mainwindow = mainwindow = self.top3 = builder.get_object("mainwindow", self.master)

        # Set main menu
        self.mainmenu = menu = builder.get_object("mainmenu", self.master)
        self.set_menu(menu)

        # Initialize model
        self.people = ListOfHumans()

        # change main window message
        label = self.builder.get_object('label_main')
        console_frame = self.builder.get_object('label_main')
        self.console = peopleGUI.console_gui.ConsoleUi(console_frame)
        formatter = logging.Formatter('%(asctime)s: %(message)s')
        self.console.queue_handler.setFormatter(formatter)
        logger.addHandler(self.console.queue_handler)

        logger.debug('debug message')
        logger.info('info message')
        logger.warning('warn message')
        logger.error('error message')
        logger.critical('critical message')

        for i in range(1, 5):
            logger.info("Hello !!!!!")

        # Configure callbacks
        builder.connect_callbacks(self)

    def on_mfile_item_clicked(self, itemid):
        if itemid == "mfile_open":

            input_file = filedialog.askopenfilename(initialdir=".", title="Select file",
                                                    filetypes=(("json files", "*.json"), ("all files", "*.*")))
            print(input_file)
            logger.info("File = {}".format(input_file))
            with open(input_file) as json_file:
                data = json.load(json_file)
                for p in data["people"]:
                    self.people.add_human(Human(p["name"], int(p["age"])))

        if itemid == "mfile_quit":
            messagebox.showinfo("File", "You clicked Quit menuitem. Byby")
            self.quit()

    def on_mdata_item_clicked(self, itemid):
        if itemid == "mdata_add":
            # LabelFrame_NewHuman

            # Method 2. Recommended way.
            # Recreate the builder and window each time.
            self.builder2 = builder2 = pygubu.Builder()

            menu_file = find_data_file("menu.ui")
            builder2.add_from_file(menu_file)

            self.top3 = top3 = tk.Toplevel(self.mainwindow)
            frame3 = builder2.get_object('LabelFrame_NewHuman', top3)

            callbacks = {
                'on_button_add_human_clicked': self.on_button_add_human_clicked
            }
            builder2.connect_callbacks(callbacks)

        if itemid == "mdata_print":
            # label = self.builder.get_object('label_main')
            # label.configure(text="People : {names_and_ages}".format(names_and_ages=self.people))
            logger.info("People in the list: {names_and_ages}".format(names_and_ages=self.people))
            logger.info("Average age: {age}".format(age=self.people.get_average_age()))

    def on_button_add_human_clicked(self):
        # change main window message

        entry_name = self.builder2.get_object('Entry_Name')
        entry_age = self.builder2.get_object('Entry_Age')
        name = entry_name.get()
        age = entry_age.get()

        self.people.add_human(Human(name, age))
        print(self.people)

        self.top3.destroy()

        label = self.builder.get_object('label_main')
        label.configure(text="Name = {name}, Age = {age}".format(name=name, age=age))

    def on_about_clicked(self):
        messagebox.showinfo("About", "You clicked About menuitem")

def find_data_file(filename):
    if getattr(sys, 'frozen', False):
        # The application is frozen
        datadir = os.path.dirname(sys.executable)
    else:
        # The application is not frozen
        # Change this bit to match where you store your data files:
        datadir = os.path.dirname(__file__)

    return os.path.join(datadir, filename)


if __name__ == "__main__":
    people = ListOfHumans()

    script_dir_full = find_data_file("logging_gui.yaml")

    with open(script_dir_full) as f:
        # use safe_load instead load
        log_cfg = safe_load(f)

    #log_cfg = safe_load(resource_stream('peopleGUI',script_dir_full))

    logging.config.dictConfig(log_cfg)
    # create (root) logger
    logger = logging.getLogger()
    #logger.setLevel(logging.DEBUG)
    logger.info("Hello in terminal")
    logger.debug("START __main__")

    root = tk.Tk()
    app = MyApplication(root)
    app.run()
    logger.debug("END __main__")