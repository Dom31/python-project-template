
# Built-in modules
from yaml import safe_load
import tkinter as tk

import tkinter.scrolledtext as scrolledtext
import logging
import logging.config
import logging.handlers
from peopleGUI.text_handler import TextHandler

from pkg_resources import resource_stream
# https://gist.github.com/moshekaplan/c425f861de7bbf28ef06
# better: https://beenje.github.io/blog/posts/logging-to-a-tkinter-scrolledtext-widget/
# Sample usage

if __name__ == '__main__':
    # Create the GUI
    root = tk.Tk()

    st = scrolledtext.ScrolledText(root, state='disabled')
    st.configure(font='TkFixedFont')
    st.pack()

    # Create textLogger
    text_handler = TextHandler(st)

    # Add the handler to logger
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.addHandler(text_handler)

    # Log some messages
    logger.debug('debug message')
    for i in range(1, 50):
        print (i)
        logger.info('info message')

    logger.warning('warn message')
    logger.error('error message')
    logger.critical('critical message')

    root.mainloop()