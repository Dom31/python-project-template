import argparse
from yaml import safe_load
import logging
import logging.config
import logging.handlers
from pkg_resources import resource_stream

import sys

from people.Human import Human
from people.ListOfHumans import ListOfHumans

"""
command line application for managing people (list of humans)

Examples:
>> people_cli file -h
>> people_cli file file -h
>> people_cli file -i <input json file>
>> people_cli init

"""


def initialize_people_from_fake_values(args) -> ListOfHumans:
    """initialize a list of humans in the people container with fake hard values

    Args:
        args: command line input arguments

    Returns:
        ListOfHumans: people with added items
    """
    people = ListOfHumans(Human("Joe", 10))
    people.add_human(Human("Toto", 47))
    return people


def initialize_people_from_file(args) -> ListOfHumans:
    """ initialize a list of humans in the people container with fake hard values in a input json file

    Args:
        args: command line input arguments containing args.input_file = Json input file

    Returns:
        ListOfHumans: people with added items
    """

    people = ListOfHumans(input_file=args.input_file)
    return people


def main():

    input("ENTER to continue....")
    log_cfg = safe_load(resource_stream('data', 'logging_gui.yaml'))
    logging.config.dictConfig(log_cfg)
    # create (root) logger
    # TODO: use loggers in yaml file as define them oas root logger (used in multiple python sub modules)
    logger = logging.getLogger()

    # 'application' code
    logger.debug('debug message')
    logger.info('info message')
    logger.warning('warn message')
    logger.error('error message')
    logger.critical('critical message')

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    add_subcommand_init_people(subparsers)
    add_subcommand_add_people_from_file(subparsers)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()

    my_people = args.func(args)
    logger.info(my_people)
    logger.info("average age = {age} ".format(age=my_people.get_average_age()))

    logger.debug("END program")
    input("ENTER to continue....")



def add_subcommand_init_people(subparsers):
    parser = subparsers.add_parser('init', help='initialize people in list')
    parser.set_defaults(func=initialize_people_from_fake_values)


def add_subcommand_add_people_from_file(subparsers):
    parser = subparsers.add_parser('file', help='add people in list from json file')
    parser.set_defaults(func=initialize_people_from_file)
    parser.add_argument('-i', '--input_file', type=str, default='data/people.son', help='input file name')


if __name__ == '__main__':
    main()
