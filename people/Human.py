import string

class Human:
    """
    class defining an human: name, age,...

    """
    def __init__(self, name: string, age: int):
        """ initialize a  human

        Args:
           name (str): human name
           age (int): human age
        """
        self.age = age
        self.name = name

    def change_age(self, age: int):
        """ change the age of the human

        Args:
           age (int): human age

        Returns:
            None
        """
        self.age = age
