import json
import logging
import os
import string
import sys

from people.Human import Human

# logger = logging.getLogger('log_in_console_and_file')
logger = logging.getLogger(__name__)

class ListOfHumans:
    """
    manage a list of humans
    """

    def __init__(self, human_to_add: Human = None, input_file: string = None):
        """ initialize the list of humans, possibly with a human given as parameter

        Args:
            human_to_add (Human): human as first item to add
            input_file (String): json file with name:age values
        """
        self.separator_output = ","
        self.humans = []
        if human_to_add:
            assert isinstance(human_to_add, Human)
            self.humans.append(human_to_add)

        if input_file:
            self.initialize_people_from_input_file(input_file)

    def __str__(self):
        return self.get_names_and_ages()

    def get_names_and_ages(self) -> str:
        """ return a ist of names and ages separated with commas

        Args:
            None
        Returns:
             str: list of names and ages separated with self.separator_output (default is ',')
        """
        human_info = []
        for human in self.humans:
            human_info.append(
                "{name}: age = {age}".format(name=human.name, age=human.age)
            )
        return self.separator_output.join(human_info)

    def add_human(self, human: Human):
        """ add a human in the list

        Args:
            human (Human): new human to be added in the list
        """
        assert isinstance(human, Human)
        self.humans.append(human)

    def initialize_people_from_input_file(self, input_file: string):
        """ read a Json file and fill the people container

        Args:
            input_file (string) path/name of a json file

        Raises:
            Quit the app if the file does not exist
            TODO: implement exception instead quitting app
        """
        logger.debug("START initialize_people_from_input_file")
        if os.path.isfile(input_file):
            with open(input_file) as json_file:
                data = json.load(json_file)
                for p in data["people"]:
                    self.add_human(Human(p["name"], int(p["age"])))
        else:
            logger.error("The file {filename} specified does not exist".format(filename=input_file))
            sys.exit()
        logger.debug("END initialize_people_from_input_file")

    def get_average_age(self) -> float:
        """ calculate the average of the age of all the humans

        Note: empty list is not considered...

        Returns:
            float: average of the age of all the humans
        """
        logger.debug("START get_average_age")
        total_age = 0
        average_age = 0
        for human in self.humans:
            total_age += human.age
        try:
            average_age = total_age / len(self.humans)
        except Exception as e:
            # logging.exception("No humans in the list !")
            logging.critical(e, exc_info=True)

        logger.debug("END get_average_age")
        return average_age
