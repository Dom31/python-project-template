# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.0.0] - 2019-11-05
### Added
 - documentation ([sphinx](http://www.sphinx-doc.org/en/stable/), selfhosted + [readthedocs](https://readthedocs.org/))
 - testing ([pytest](https://docs.pytest.org/en/latest/))
 - building a package (`setup.py`, `README.md`, `CHANGELOG.md`, `LICENSE.md`)
 - command line interface with argparse

### Changed

### Removed
