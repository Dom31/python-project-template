peopleGUI package
=================

Submodules
----------

peopleGUI.console\_gui module
-----------------------------

.. automodule:: peopleGUI.console_gui
   :members:
   :undoc-members:
   :show-inheritance:

peopleGUI.demo\_gui module
--------------------------

.. automodule:: peopleGUI.demo_gui
   :members:
   :undoc-members:
   :show-inheritance:

peopleGUI.people\_gui module
----------------------------

.. automodule:: peopleGUI.people_gui
   :members:
   :undoc-members:
   :show-inheritance:

peopleGUI.queue\_handler module
-------------------------------

.. automodule:: peopleGUI.queue_handler
   :members:
   :undoc-members:
   :show-inheritance:

peopleGUI.textHandler module
----------------------------

.. automodule:: peopleGUI.textHandler
   :members:
   :undoc-members:
   :show-inheritance:

peopleGUI.text\_handler module
------------------------------

.. automodule:: peopleGUI.text_handler
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: peopleGUI
   :members:
   :undoc-members:
   :show-inheritance:
