people package
==============

Submodules
----------

people.Human module
-------------------

.. automodule:: people.Human
   :members:
   :undoc-members:
   :show-inheritance:

people.ListOfHumans module
--------------------------

.. automodule:: people.ListOfHumans
   :members:
   :undoc-members:
   :show-inheritance:

people.people\_cli module
-------------------------

.. automodule:: people.people_cli
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: people
   :members:
   :undoc-members:
   :show-inheritance:
