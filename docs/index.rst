.. project python sample documentation master file, created by
   sphinx-quickstart on Mon Nov  4 11:22:23 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to project python sample's documentation!
=================================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   changelog




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
