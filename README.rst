Demo gitlab project
====================

``oneClass`` is a Python package providing example of a complete and simple python project


Installation
============

Requirements
------------

``oneClass`` no depends on any python packages (yes !)


Install with git
----------------

.. code-block:: git

   git clone https://github.com/Dom31/oneClass/oneClass.git


Using the project
=================

How to build
------------

``no build instructions``

How to use
----------

.. code-block:: python

    python src/hello.py

or run it directly from the IDE as Pycharm

.. code-block:: console

    Pycharm > Run > Hello.py


Bug reports
===========

Please report bugs and feature requests at
https://github.com/Dom31/oneClass/issues


Documentation
=============

The full documentation for CLI and API is available on `readthedocs
<http://oneClass.readthedocs.org/en/stable/>`_.

Build the docs
--------------
You can build the documentation using ``sphinx``::

Sphinx installation:

.. code-block:: console

    pip install sphinx
    python setup.py build_sphinx
    
Project documentation:

.. code-block:: console

    cd docs
    sphinx-apidoc -f -o source/ ../src
    make clean
    make html

Contributing
============

You can contribute to the project in multiple ways:

* Write documentation
* Implement features
* Fix bugs
* Add unit and functional tests
* Everything else you can think of

We enforce commit messages to be formatted using the `conventional-changelog <https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines>`_.
This leads to more readable messages that are easy to follow when looking through the project history.

Please provide your patches as github pull requests. Thanks!

Code-Style
----------

We use black as code formatter, so you'll need to format your changes using the
`black code formatter
<https://github.com/python/black>`_.

Just run

.. code-block:: console

    cd python-gitlab/
    pip3 install --user black
    black .
  
to format your code according to our guidelines.

Running unit tests
------------------

Before submitting a pull request make sure that the tests still succeed with
your change. Unit tests and functional tests run using the travis service and
passing tests are mandatory to get merge requests accepted.


   # run the unit tests

.. code-block:: python

    python -m unittest




