from unittest import TestCase
import os

from people.Human import Human
from people.ListOfHumans import ListOfHumans


class TestListOfHumans(TestCase):

    def test_init(self):
        joe = Human("Joe", 45)
        people = ListOfHumans(joe)
        self.assertEqual(people.humans[0].age, 45)

    def test_add_human(self):
        joe = Human("Joe", 45)
        people = ListOfHumans(joe)

        people.add_human(Human("Toto", 47))
        self.assertEqual(people.humans[1].age, 47)


class TestListOfHumans(TestCase):
    def test_initialize_people_from_input_file(self):
        input_file = os.path.join(os.path.dirname(__file__), './data/people-data-test.json')

        people = ListOfHumans(input_file=input_file)
        self.assertEqual(people.humans[1].age, 50)
        self.assertEqual(people.humans[0].name, "Joe")

