from unittest import TestCase

from people.Human import Human


class TestHuman(TestCase):
    def test_init(self):
        joe = Human("Joe", 45)
        self.assertEqual (joe.age,45)