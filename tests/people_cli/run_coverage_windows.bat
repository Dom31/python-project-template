set /p temp="Hit enter to continue..."
echo"Current directory :" %cd%
set working_directory=%cd%
set browser_binary="C:\Program Files\Mozilla Firefox\firefox.exe"
set coverage_binary="C:\Users\Euclid\Documents\Anaconda\Scripts\coverage-3.7.exe"
REM %coverage_binary%  run --rcfile=%working_directory%\tests\.coveragerc -m unittest discover -s tests/
%coverage_binary% run -m unittest discover -s tests/
%coverage_binary% report -m
%coverage_binary% html
%browser_binary%  %working_directory%\htmlcov\index.html