# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path
import os.path
#
import cx_Freeze
import sys



PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')

base = None

if sys.platform=='win32':
    base = "Win32GUI"

icone = None
if sys.platform == "win32":
    icone = "icone.ico"

executables = [cx_Freeze.Executable("peopleGUI/demo_gui.py"),cx_Freeze.Executable("peopleGUI/people_gui.py")]

cx_Freeze.setup(
        name = "people_gui",
        options = {"build_exe":{
            "packages":["tkinter","re","pygubu"],
            'include_files':[
                os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
                os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'),
                'peopleGUI\logging_gui.yaml',
                'people/logging.yaml',
                'peopleGUI/menu.ui'
            ],
            "zip_include_packages": "*",
            "zip_exclude_packages": "",
        }
                   },
        version="0.01",
executables=executables)


here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='people',
    version='1.0.0',
    description='A python package template',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://a-python-project-template.readthedocs.io/en/latest/index.html',
    author='Dom31',
    author_email='toutpres@yahoo.fr',
    license="MIT",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7'
    ],
    keywords='python package template documentation testing continuous deployment',
    packages=find_packages(exclude=['docs', 'tests','data','build','htmlcov']),
    package_data = { 'data' : ['*.yaml', '*.json', '*.ui'] },
    setup_requires=['pytest-runner', 'setuptools>=38.6.0'],  # >38.6.0 needed for markdown README.md
    tests_require=['pytest', 'pytest-cov'],
    entry_points={
        'console_scripts': [
            'helloworld=pypkgtemp.__main__:main'
        ]
    }, install_requires=['pygubu','m2r', 'PyYAML']
)



